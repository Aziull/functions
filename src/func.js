const getSum = (str1, str2) => {
  if (
    typeof str1 !== "string" ||
    typeof str2 !== "string" ||
    isNaN(str1) ||
    isNaN(str2)
  ) {
    return false;
  }
  const int1 = str1 === "" ? 0 : Number.parseInt(str1 + "n");
  const int2 = str2 === "" ? 0 : Number.parseInt(str2 + "n");
  return (int1 + int2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const output = {
    post: 0,
    comments: 0
  }
  listOfPosts.forEach(post => {
    if(post.author === authorName){
      output.post++
    }
    if(post.comments){
      post.comments.forEach(comment => {
      if(comment.author === authorName){
        output.comments++
      }
    })
    }
  });
  return `Post:${output.post},comments:${output.comments}`;
};

const tickets = (people) => {
  let prev = 0;
  for (let cur of people) {
    if (cur === 25) {
      prev += cur;
      continue;
    }

    if (prev - (cur - 25) < 0) {
      return "NO";
    }
    prev += 25;
  }
  return "YES";
};
tickets([25, 50, 100]);
module.exports = { getSum, getQuantityPostsByAuthor, tickets };
